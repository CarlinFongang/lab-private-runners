# Gitlab-CI | Mise en place de runner privé | Mise en place d'un pipeline CI : build d'image docker embarquant apinehelloworld
<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytrainingfr/alpinehelloworld.git" alt="Crédit : eazytraining.fr" >
  </a>
</p>

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/




## Redaction du fichier .gitlab-ci.yml
une fois le fichier modifier, faire un commit et push pour l'ajouter au repo
>![Alt text](img/image.png)

## Mise en route du pipeline CICD pour le build 
>![Alt text](img/image-1.png)

## consultation des logs 
>![Alt text](img/image-2.png)
>![Alt text](img/image-3.png)

## Mise en place de runner privés
`mkdir -p /data/gitlab/runner/`

## lancement d'un conteneur docker privé qui va servir de runner gitlab
doc : https://docs.gitlab.com/runner/install/docker.html
````
docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /data/gitlab/runner:/etc/gitlab-runner gitlab/gitlab-runner:latest
````
>![Alt text](img/image-4.png)
>![Alt text](img/image-5.png)

## Export du token gitlab pour l'authentification du runner privé
`export TOKEN=<token_a_récupérer_sur_gitlab>`

## Lancement du register privé
doc : https://docs.gitlab.com/runner/register/?tab=Docker#

````
docker run --rm -v /data/gitlab/runner/:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image docker:dind \
  --url "https://gitlab.com/" \
  --registration-token $TOKEN \
  --description "docker-runner-carlinfg" \
  --tag-list "carlinfg" \
  --run-untagged="true" \
  --locked="false" \
  --maintenance-note "For maintenance : fongangcarlin@gmail.com" \
  --access-level="not_protected" \
  --docker-privileged \
  --docker-volumes '/var/run/docker.sock:/var/run/docker.sock'
````
>![Alt text](img/image-6.png)

## Vérification de la connexion au runner privé
>![Alt text](img/image-7.png)

## Ajout du tag relatif au nouveau runner dans le code de lancement du pipeline
>![Alt text](img/image-11.png)

## Test tu pipeline avec le nouveau runner privé : new commit and push
>![Alt text](img/image-9.png)
>![Alt text](img/image-10.png)

## exécution des jobs
![Alt text](img/image-12.png)